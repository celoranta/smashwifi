SmashWifi
Version: 0.1 (Incomplete)

IMPORTANT NOTE: Please read the 'Known Issues.'  This code was created as proof of feasibility ONLY and is not yet complete; although 
it can be simulated by using OS X to run both of the files as explained in 'OPERATION.' The SmashWifiClient app should be copied to 
a USB stick and run as a portable app for testing, while SmashWifiServer can be run on OS X terminal command line.

SmashWifi is a system of related apps which allow end users to add and update wireless network settings to SmashPotatoes PC Sticks 
without requiring the end user to disconnect the PC stick or connect keyboards, mice, or other peripherals.


The typical use cases occur when:
	A Smashpotatoes video streaming device arrives at an install location without valid preconfiguration of wireless settings
	The wireless network configuration at the installation site is updated
	The Streaming Device's wireless configuration settings has its 'Remember This Network' option unchecked


SmashWifi is comprised of:

	-SmashWifiServer:  A bash script to be installed on every PC Stick as part of the .ISO image, along with a uDev rule to launch it
	-SmashWifiClient(s): Platform-specific executables to be loaded on a cheap, low-profile USB flash drive and shipped pre-connected to each PC stick. 


OPERATION:

INITIAL SETUP (to be performed during PC Stick setup process, prior to deployment; while keyboard and mouse are still available and connected)
NOTE: Incomplete Version... valid for MAC OS X only, and must be run manually (not automatically) in all cases.

1. Insert a USB flash drive into the PC Stick
2. In a bash shell, navigate to the working directory of the SmashWifiServer app 
3. Run the app with ./put-decode-wifihash -t
4. The terminal will list all mounted drives.  Enter the directory path of the inserted USB drive at the prompt:  (e.g. /Volumes/EMPTY on Mac or /Dev/Empty on linux)
5. The app will 'tag' the removable drive with a unique file, signifying that it is to be recognized 
	as a SmashWifiDevice upon mounting in a SmashWifi Streaming Device
6. (Future) The streaming device will reboot itself, and the -m function should run automatically upon mounting of the USB stick during startup 

KEY GENERATION - FULLY AUTOMATED (To be run automatically upon the mounting of any volume on a SmashPotatoes Streaming Device filesystem)

(Incomplete function... must launch ./put-decode-wifihash -m <SmashWifiDevice Directory Path> in OS X to simulate)
1. The script will generate a security key and write it to the SmashWifiDevice on a hidden file
2. The script will look for an encoded network settings file.  Finding none, it will unmount the SmashWifiDevice and close its shell.
3. SmashPotatoes streaming device will operate normally.
4. The SmashWifiDevice will remain physically seated in the USB port (unmounted) until it is required.

OBTAIN WIFI SETTINGS

1. The end user will remove the SmashWifiDevice (USB drive) from the SmashPotatoes Streaming Device's USB port and place it in the 
	USB port of the end user's Mac (Future: or PC.)
2. The end user will click-to-run the executable found on the USB Drive
3. A bash prompt (future: Welcome Screen) will open and prompt the user to choose 'manual' or 'automatic' mode.
4. If manual mode is selected, the user will enter their wireless network settings at the prompt.  
	note: There should be NO administrator password requirement during manual mode
5. If automatic mode is selected, the user will enter their administrator name and password (potentially several times as required by permissions)
6. The entered or discovered wireless network information will be encoded and written to a hidden file on the SmashWifiDevice
7. Upon completion the user will be prompted to eject the SmashWifiDevice and plug it back into the Streaming Device.

LOAD WIFI SETTINGS

1. Upon hot plugging the USB into the streaming device, the SmashWifiServer app will detect the encoded network 
	settings file (after replacing the security key as detailed in an entry above)
2. The network settings will be decoded and the results will be written (FUTURE) to a new wireless configuration file.  
	The new configuration will be set to 'Remember this network' = TRUE, and set to high priority.
3. (FUTURE) The network manager daemon will be restarted to load the new settings.
4. (Future) The app will attempt to post a message to our server, to notify SmashPotatoes of the successful connection
5. The SmashWifiDevice will be unmounted.  (It is now ready for another attempt, or for use the next time the customer's wireless configuration is changed.)
6. The app will exit runtime


PARTIAL LIST OF SmashWifiServer runtime operation:

 1.  A udev rule on the PC Stick will launch the script with the -m option each time a USB device is mounted; with the directory of the mounted filesystem as the parameter
2. The script will check the mounted directory for the 'tag' file; exiting immediately if no tag file is present.
3. The remainder of the script will detach itself from the active bash shell (to allow the udev process to continue in the background.)
4. The script will load the previous security key from the PC stick's local 'key' file into runtime memory, and will replace the file with a newly generated file containing a new key
  5.  The script will write the newly generated security key to an invisible file on the SmashWifiDevice.



Known issues:
	The SmashWifiServer client has been written in the bash shell of a Mac, and has not been ported to or tested on Ubuntu linux.
	The SmashWifiServer client will require the addition of a udev rule to the SmashPotatoes .ISO image to run the script each time a device is mounted to the filesystem, with the root directory of the mounted filesystem passed as a parameter to the SmashWifiServer app using the -m option.  (This has been researched but will require some additional research as well as implementation)
 	Permissions handling needs to be reviewed to ensure that the programs run smoothly for the users, and to ensure that the 'Manual Entry' option of the SmashWifiClient apps do NOT require authentication.
	SmashWifiServer should check the supplied directory to ensure it is the root directory of a mounted filesystem (it currently just checks whether it has been passed a valid directory)
	When writing to wicd, 'Remember this network' should be set to TRUE and the new network should be given top priority
	Some of the 'type n to exit' prompts do not properly exit the app
	SmashWifiServer -t procedure should include a warning 'This drive will no longer function as  removable memory when used in SmashPotatoes Video Streaming PCs.  Is this OK?
	SmashWifiClient will not run on certain older versions of OS X.  (the symlink to the Airport requires a different path.)  A check for OS version will need to be added and the alternate path loaded into the variable.
	A Windows version of the SmashWifiClient will need to be created.  This is possible as per my research, and should follow a similar process as the Mac version.
